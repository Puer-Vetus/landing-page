const fetch = require('isomorphic-fetch')
const seedrandom = require('seedrandom')

exports.handler = async (event, context) => {
  const { username } = event.queryStringParameters
  console.log(username)

  // const number = Math.floor(seedrandom(username) * 30 + 1)
  const numberGenerator = seedrandom(username)
  const number = Math.floor(numberGenerator() * 30 + 1)
  console.log(number)

  const fileURL = `https://mapsupport.club/avatars/${number}.jpg`

  console.log(fileURL)

  let image
  try {
    const result = await fetch(fileURL)
    image = await result.buffer()
  } catch (error) {
    console.log('error', error)
    return {
      statusCode: 500,
      body: JSON.stringify({
        error: error.message,
      }),
    }
  }

  return {
    statusCode: 200,
    headers: {
      'Content-type': 'image/jpeg',
    },
    body: image.toString('base64'),
    isBase64Encoded: true,
  }
}
