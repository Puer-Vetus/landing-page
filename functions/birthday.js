const fetch = require('isomorphic-fetch')
const Jimp = require('jimp')

let cache = {}

exports.handler = async (event, context) => {
  const { username } = event.queryStringParameters
  console.log(username)

  let buffer = undefined
  let MIME = undefined
  console.log('Cache is currently: ', cache)
  if (cache[username]) {
    console.log('Using cache')
    buffer = cache.username.buffer
    MIME = cache.username.MIME
  } else {
    console.info('Retrieving avatar...')

    const profile_avatar_url = `https://chat.mapsupport.club/avatar/${username}?rc_uid=XgHsc6JaAu9HgtaDb&rc_token=jlVRQ50SSsunsbu6I6ORqFHSjdAio_EfvnXXilAWNnx`

    console.info('Retrieving Balloons...')
    const balloons_url = `https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.pinimg.com%2Foriginals%2F8b%2F9a%2Fd0%2F8b9ad0e6e8ded5b7af68014250a782c5.png&f=1&nofb=1`

    let profile_avatar
    try {
      profile_avatar = await Jimp.read(profile_avatar_url).then((image) => {
        return image.resize(256, 256)
      })
    } catch (e) {
      profile_avatar = await Jimp.read(
        `https://mapsupport.club/.netlify/functions/avatar?username=${username}`,
      ).then((image) => {
        return image.resize(256, 256)
      })
    }

    let balloons = await Jimp.read(balloons_url).then((image) => {
      return image.resize(256, 300)
    })

    profile_avatar.composite(balloons, 0, 0)

    MIME = profile_avatar.getMIME()
    buffer = await profile_avatar.getBufferAsync(MIME)

    cache[username] = {
      buffer,
      MIME,
    }
  }

  console.log(buffer)

  return {
    statusCode: 200,
    headers: {
      'Content-type': MIME,
    },
    body: buffer.toString('base64'),
    isBase64Encoded: true,
  }
}
